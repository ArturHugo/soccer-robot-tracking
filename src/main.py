import cv2 as cv
import numpy as np
from include.TrackerUtils import DetectRobots, CreateTracker


src_folder = "data/circles/"
digit_format = "05d"
COLOR_1 = np.array([255,0,255])
RADIUS = 15
multitracker = cv.MultiTracker_create()
tracker_name = cv.TrackerMedianFlow_create
bboxes = None

frame_counter = 0
while 1:
    # Reading frame from source folder.
    filename = format(frame_counter, digit_format)
    frame = cv.imread("{}{}.jpg".format(src_folder, filename), 1)

    # If file not found, the video has ended.
    if frame is None:
        break

    # Initializes tracker if no bounding boxes are tracked.
    if bboxes is None:
        bboxes = DetectRobots(frame, COLOR_1, RADIUS)
        for bbox in bboxes:
            multitracker.add(CreateTracker(tracker_name), frame, bbox)
    else:
        success, bboxes = multitracker.update(frame)
        print(success)
        for bbox in bboxes:
            p1 = (int(bbox[0]), int(bbox[1]))
            p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
            cv.rectangle(frame, p1, p2, [0,0,0], 1, 1)


    # Updating frame, window and getting keyboard input for quitting.
    frame_counter += 1

    cv.imshow('video', frame)
    key = cv.waitKey(0) & 0xFF

    if key == ord('q'):
        break
