import cv2 as cv
import numpy as np


# Tracking.
def CreateTracker(tracker_type):
    return tracker_type()

# Detection and segmentation.
def RemoveBG(img, bg_color, tolerance = 40):
    """
    Returns foreground elements thresholding the background out.
    """

    lower_lim = bg_color - tolerance
    upper_lim = bg_color + tolerance

    mask = np.zeros(img.shape[:2])
    mask = cv.inRange(img, lower_lim, upper_lim)

    return 255-mask

def SegmentByColor(img, color, tolerance = 40):    
    """
    Returns elements of the chosen color.
    """

    lower_lim = color - tolerance
    upper_lim = color + tolerance

    mask = np.zeros(img.shape[:2])
    mask = cv.inRange(img, lower_lim, upper_lim)

    return np.uint8(mask)

def GetObjectBBox(object_mask):
    """
    Takes a mask which tells where the object is in the image and return
    it's corresponding bounding box.
    """
    # Get the index of the first occurence of non-zero value accross each axis.
    xmin = np.argmax(np.max(object_mask, axis=0))
    ymin = np.argmax(np.max(object_mask, axis=1))

    print(xmin, ymin)

    # Get the index of the last occurence of non-zero value accross each axis.
    width, height = object_mask.shape
    noisex, noisey = np.mgrid[range(width),range(height)] * 1e-15
    xmax = np.argmax(np.max(object_mask + noisex + noisey, axis=0))
    ymax = np.argmax(np.max(object_mask + noisex + noisey, axis=1))

    width, height = xmax - xmin, ymax - ymin
    bbox = tuple([xmin, ymin, width, height])
    return bbox

def DetectRobots(img, color, radius, tolerance = 40):
    """
    Return bouding boxes around elements of the chosen color.
    Inputs: an image, color of the elements, approximate radius of the elements
    and thresholding tolerance to segment the color.
    """
    # Segmenting each element present in the image,
    mask = SegmentByColor(img, color, tolerance=tolerance)
    num_components, components = cv.connectedComponents(mask)

    # Removes noise from the segmentation.
    kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (radius,radius))
    components = cv.morphologyEx(np.uint8(components), cv.MORPH_OPEN, kernel)

    cv.imshow('comps', np.uint8(components*255/num_components))

    bboxes = []
    # For each element, give it it's own bounding box.
    for label in np.unique(components)[1:]:
        component_mask = np.uint8(np.where(components == label, 1, 0))
        bbox = GetObjectBBox(component_mask)
        bboxes.append(bbox)

    return bboxes